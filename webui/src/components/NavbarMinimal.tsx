import { useEffect, useState } from "react";
import { Tooltip, UnstyledButton, Stack, rem } from "@mantine/core";
import {
  IconHome2,
  IconSettings,
  IconLogout,
  IconSwitchHorizontal,
  IconArrowUpCircle,
} from "@tabler/icons-react";
import classes from "./NavbarMinimal.module.css";
import pb from "../lib/pocketbase";
import { Navigate, useNavigate } from "react-router-dom";

interface NavbarLinkProps {
  icon: typeof IconHome2;
  label: string;
  active?: boolean;
  onClick?(): void;
}

function NavbarLink({ icon: Icon, label, active, onClick }: NavbarLinkProps) {
  return (
    <Tooltip label={label} position="right" transitionProps={{ duration: 0 }}>
      <UnstyledButton
        onClick={onClick}
        className={classes.link}
        data-active={active || undefined}
      >
        <Icon style={{ width: rem(20), height: rem(20) }} stroke={1.5} />
      </UnstyledButton>
    </Tooltip>
  );
}

enum Pages {
  Home = 0,
  Deployments,
  Settings,
}

const mockdata = [
  { icon: IconHome2, label: "Home" },
  { icon: IconArrowUpCircle, label: "Deployments" },
  { icon: IconSettings, label: "Settings" },
];

export function NavbarMinimal() {
  const [active, setActive] = useState<number>(Pages.Home);
  const [loggedOut, setLoggedOut] = useState<boolean>(false);
  const navigate = useNavigate();

  useEffect(() => {
    switch (active) {
      case Pages.Deployments:
        navigate("/deployments");
        break;
    }
  }, [active]);

  const links = mockdata.map((link, index) => (
    <NavbarLink
      {...link}
      key={link.label}
      active={index === active}
      onClick={() => setActive(index)}
    />
  ));

  return (
    <nav className={classes.navbar}>
      {loggedOut && <Navigate to="/login"></Navigate>}
      <div className={classes.navbarMain}>
        <Stack justify="center" gap={0}>
          {links}
        </Stack>
      </div>

      <Stack justify="center" gap={0}>
        <NavbarLink icon={IconSwitchHorizontal} label="Change account" />
        <NavbarLink
          icon={IconLogout}
          label="Logout"
          onClick={() => {
            pb.authStore.clear();
            setLoggedOut(true);
          }}
        />
      </Stack>
    </nav>
  );
}
