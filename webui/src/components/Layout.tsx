import { ReactNode } from "react";
import { AppShell, Group, Container } from "@mantine/core";
import { NavbarMinimal } from "./NavbarMinimal";

type LayoutProps = {
  children?: ReactNode;
};

export default function Layout(props: LayoutProps) {
  const { children } = props;

  return (
    <AppShell header={{ height: 60 }} padding="md">
      <AppShell.Header>
        <Group h="100%" px="md">
          Deployer
        </Group>
      </AppShell.Header>
      <AppShell.Navbar>
        <NavbarMinimal></NavbarMinimal>
      </AppShell.Navbar>
      <AppShell.Main>
        <Container>{children}</Container>
      </AppShell.Main>
    </AppShell>
  );
}
