import { RecordAuthResponse } from "pocketbase";

export type CreateUserReq = {
  email: string;
  password: string;
  passwordConfirm: string;
};

export type CreateUserResp = {
  email: string;
};

export type LoginReq = {
  email: string;
  password: string;
};

export type LoginResp = RecordAuthResponse;

export type Deployment = {
  id: string;
  name: string;
};

export type CreateDeploymentReq = {
  name: string;
  author: string;
};
