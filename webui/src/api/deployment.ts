import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import pb from "../lib/pocketbase";
import { Deployment, CreateDeploymentReq } from "./types";

export const getDeploymentKeys = {
  getDeploymentsByUserId: (userId: string) =>
    [{ userId }, "deployments"] as const,
};

const fetchDeployments = async () => {
  const rows = await pb
    .collection("deployments")
    .getFullList<Deployment>({ sort: "-created" });

  return rows;
};

export const useGetDeployments = (userId: string) => {
  return useQuery({
    queryKey: getDeploymentKeys.getDeploymentsByUserId(userId),
    queryFn: () => fetchDeployments(),
  });
};

export const useCreateDeployment = (userId: string) => {
  const queryClient = useQueryClient();

  return useMutation<CreateDeploymentReq, {}, {}>({
    mutationFn: (data) => pb.collection("deployments").create(data),
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: getDeploymentKeys.getDeploymentsByUserId(userId),
      });
    },
  });
};
