import { useMutation } from "@tanstack/react-query";
import { LoginResp, LoginReq, CreateUserReq, CreateUserResp } from "./types";
import pb from "../lib/pocketbase";

const useLogin = () => {
  return useMutation<LoginResp, {}, LoginReq>({
    mutationFn: (data) =>
      pb.collection("users").authWithPassword(data.email, data.password),
  });
};

const useRegisterUser = () => {
  return useMutation<CreateUserResp, {}, CreateUserReq>({
    mutationFn: (data) => pb.collection("users").create(data),
  });
};

export { useLogin, useRegisterUser };
