import "@mantine/core/styles.css";
import { ComponentType, ReactNode } from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { MantineProvider } from "@mantine/core";
import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from "react-router-dom";
import { theme } from "./theme";
import ErrorPage from "./pages/ErrorPage";
import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import DeploymentPage from "./pages/DeploymentPage";

import pb from "./lib/pocketbase";

function isAuthenticated(): boolean {
  return pb.authStore.isValid;
}

function RequiredAuth({ as: Comp, ...rest }: { as: ComponentType }): ReactNode {
  if (!isAuthenticated()) {
    return <Navigate to="/login" />;
  }

  return <Comp {...rest} />;
}

const router = createBrowserRouter([
  {
    path: "/",
    element: <RequiredAuth as={HomePage} />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/login",
    element: <LoginPage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/deployments",
    element: <RequiredAuth as={DeploymentPage} />,
    errorElement: <ErrorPage />,
  },
]);

const queryClient = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <MantineProvider theme={theme}>
        <RouterProvider router={router} />
      </MantineProvider>
    </QueryClientProvider>
  );
}
