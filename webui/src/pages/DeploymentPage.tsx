import {
  Title,
  Stack,
  Table,
  Flex,
  Button,
  Modal,
  TextInput,
  LoadingOverlay,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { useForm } from "@mantine/form";

import pb from "../lib/pocketbase";
import Layout from "../components/Layout";
import { useGetDeployments, useCreateDeployment } from "../api/deployment";

export default function DeploymentPage() {
  const [opened, { open, close }] = useDisclosure(false);
  const { data: deployments, isLoading } = useGetDeployments(
    pb.authStore.model?.id ?? "",
  );
  const createDeployment = useCreateDeployment(pb.authStore.model?.id ?? "");

  const form = useForm({
    initialValues: {
      name: "",
    },

    validate: {
      name: (value) => (value === "" ? "Invalid name" : null),
    },
  });

  return (
    <>
      <Modal opened={opened} onClose={close} title="New Deployment">
        <form
          onSubmit={form.onSubmit(async (values) => {
            const data = {
              name: values.name,
              author: pb.authStore.model?.id,
            };
            createDeployment.mutate(data);
            form.reset();
          })}
        >
          <LoadingOverlay
            visible={isLoading}
            zIndex={1000}
            overlayProps={{ radius: "sm", blur: 2 }}
          />
          <Stack align="flex-start">
            <TextInput
              label="Name"
              placeholder="Enter your name"
              {...form.getInputProps("name")}
            />
            <Button type="submit">Submit</Button>
          </Stack>
        </form>
      </Modal>
      <Layout>
        <Stack>
          <Title order={2}>Deployments</Title>
          <Flex
            mih={50}
            gap="md"
            justify="flex-end"
            align="flex-start"
            direction="row"
            wrap="wrap"
          >
            <Button onClick={open}>New</Button>
          </Flex>
          <Table>
            <Table.Thead>
              <Table.Tr>
                <Table.Th>Name</Table.Th>
                <Table.Th>Status</Table.Th>
              </Table.Tr>
            </Table.Thead>
            <Table.Tbody>
              {deployments &&
                deployments.map((row, index) => (
                  <Table.Tr key={index}>
                    <Table.Td>{row.name}</Table.Td>
                    <Table.Td>OK</Table.Td>
                  </Table.Tr>
                ))}
            </Table.Tbody>
          </Table>
        </Stack>
      </Layout>
    </>
  );
}
