import { Title, Text, Button, Container, Group } from "@mantine/core";
import { useRouteError } from "react-router-dom";
import classes from "./ErrorPage.module.css";

type Error = {
  statusText: string
  message: string
}

export default function ErrorPage() {
  const error = useRouteError();

  return (
    <Container className={classes.root}>
      <div className={classes.label}>Oops</div>
      <Title className={classes.title}>Sorry, an unexpected error has occurred.</Title>
      <Text c="dimmed" size="lg" ta="center" className={classes.description}>
        {(error as Error).statusText || (error as Error).message}
      </Text>
      <Group justify="center">
        <Button variant="subtle" size="md">
          Take me back to home page
        </Button>
      </Group>
    </Container>
  );
}
