import { useToggle, upperFirst } from "@mantine/hooks";
import { useForm } from "@mantine/form";
import {
  Container,
  TextInput,
  PasswordInput,
  Text,
  Paper,
  Group,
  PaperProps,
  Button,
  Divider,
  Checkbox,
  Anchor,
  Stack,
  ButtonProps,
  LoadingOverlay,
} from "@mantine/core";
import { IconBrandGithub } from "@tabler/icons-react";
import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";

import pb from "../lib/pocketbase";
import { useLogin, useRegisterUser } from "../api/auth";
import { CreateUserReq } from "../api/types";

function GithubButton(
  props: ButtonProps & React.ComponentPropsWithoutRef<"button">,
) {
  return (
    <Button
      leftSection={
        <IconBrandGithub style={{ width: "0.9rem", height: "0.9rem" }} />
      }
      variant="default"
      {...props}
    />
  );
}

export default function LoginPage(props: PaperProps) {
  const [type, toggle] = useToggle(["login", "register"]);
  const [loggedIn, setLoggedIn] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const registerUser = useRegisterUser();
  const login = useLogin();

  useEffect(() => {
    if (loggedIn) {
      setLoading(false);
    }
  }, [loggedIn]);

  const form = useForm({
    initialValues: {
      email: "",
      name: "",
      password: "",
      terms: true,
    },

    validate: {
      email: (val) => (/^\S+@\S+$/.test(val) ? null : "Invalid email"),
      password: (val) =>
        val.length <= 6
          ? "Password should include at least 6 characters"
          : null,
    },
  });

  return (
    <Container size="xs">
      {loggedIn && <Navigate to="/" />}
      <Paper radius="" mt="md" {...props}>
        <Text size="lg" fw={500}>
          Welcome
        </Text>
        <Group grow mb="md" mt="md">
          <GithubButton
            radius="xl"
            onClick={async () => {
              const authData = await pb
                .collection("users")
                .authWithOAuth2({ provider: "github" });
              setLoggedIn(authData.token.length > 0);
            }}
          >
            Github
          </GithubButton>
        </Group>

        <Divider
          label="Or continue with email"
          labelPosition="center"
          my="lg"
        ></Divider>

        <form
          onSubmit={form.onSubmit(async () => {
            setLoading(true);

            if (type === "register") {
              const data: CreateUserReq = {
                email: form.values.email,
                passwordConfirm: form.values.password,
                password: form.values.password,
              };
              const record = await registerUser.mutateAsync(data);

              const authData = await login.mutateAsync({
                email: record.email,
                password: data.password,
              });

              setLoggedIn(authData.token.length > 0);
            } else if (type === "login") {
              const authData = await login.mutateAsync({
                email: form.values.email,
                password: form.values.password,
              });

              setLoggedIn(authData.token.length > 0);
            }
          })}
        >
          <LoadingOverlay
            visible={loading}
            zIndex={1000}
            overlayProps={{ radius: "sm", blur: 2 }}
          ></LoadingOverlay>
          <Stack>
            {type === "register" && (
              <TextInput
                label="Name"
                placeholder="Your name"
                value={form.values.name}
                onChange={(event) =>
                  form.setFieldValue("name", event.currentTarget.value)
                }
                radius="md"
              ></TextInput>
            )}

            <TextInput
              required
              label="Email"
              placeholder="hello@deployer.dev"
              value={form.values.email}
              onChange={(event) =>
                form.setFieldValue("email", event.currentTarget.value)
              }
            ></TextInput>

            <PasswordInput
              required
              label="Password"
              placeholder="Your password"
              value={form.values.password}
              onChange={(event) =>
                form.setFieldValue("password", event.currentTarget.value)
              }
              error={
                form.errors.password &&
                "Password should include at least 6 characters"
              }
              radius="md"
            ></PasswordInput>

            {type === "register" && (
              <Checkbox
                label="I accept terms and conditions"
                checked={form.values.terms}
                onChange={(event) =>
                  form.setFieldValue("terms", event.currentTarget.checked)
                }
              ></Checkbox>
            )}
          </Stack>

          <Group justify="space-between" mt="xl">
            <Anchor
              component="button"
              type="button"
              c="dimmed"
              onClick={() => toggle()}
              size="xs"
            >
              {type === "register"
                ? "Already have an account? Login"
                : "Don't have an account? Register"}
            </Anchor>
            <Button type="submit" radius="xl">
              {upperFirst(type)}
            </Button>
          </Group>
        </form>
      </Paper>
    </Container>
  );
}
