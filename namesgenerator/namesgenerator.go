package namesgenerator

import (
	"math/rand"
	"strings"
	"time"
)

var (
	left = [...]string{"admiring", "agitated", "angry", "awesome", "beautiful", "boring", "brave", "clever", "cocky", "compassionate", "condescending", "cranky", "desperate", "determined", "eloquent", "epic", "fervent", "focused", "friendly", "frosty", "gallant", "gifted", "goofy", "gracious", "happy", "hardcore", "hopeful", "hungry", "infallible", "inspiring", "jolly", "keen", "kind", "laughing", "loving", "mad", "modest", "mystifying", "nervous", "nostalgic", "pensive", "practical", "priceless", "quirky", "recursing", "relaxed", "reverent", "romantic", "sad", "serene", "sharp", "silly", "sleepy", "stoic", "stupefied", "suspicious", "tender", "thirsty", "trusting", "unruffled", "upbeat", "vibrant", "vigilant", "wizardly", "wonderful", "xenodochial", "youthful", "zealous", "zen"}

	right = [...]string{"albattani", "allen", "almeida", "agnesi", "archimedes", "ardinghelli", "aryabhata", "austin", "babbage", "banach", "bardeen", "bartik", "bassi", "beaver", "bell", "benz", "bhabha", "bhaskara", "black", "blackburn", "blackwell", "bohr", "booth", "borg", "bose", "bouman", "boyd", "brahmagupta", "brattain", "brown", "buck", "burnell", "cannon", "carson", "cartwright", "carver", "cerf", "chandrasekhar", "chaplygin", "chatelet", "chaum", "clarke", "colden", "cori", "cray", "curran", "curie", "darwin", "davinci", "dewdney", "dubinsky", "easley", "edison", "einstein", "elbakyan", "elgamal", "elion", "ellis", "engelbart", "euclid", "euler", "faraday", "feistel", "fermat", "fermi", "feynman", "franklin", "gagarin", "galileo", "gates", "gauss", "germain", "goldberg", "goldstine", "goldwasser", "golick", "goodall", "gould", "greider", "grothendieck", "haibt", "hamilton", "haslett", "hawking", "hellman", "heisenberg", "hermann", "herschel", "hertz", "heyrovsky", "hodgkin", "hofstadter", "hoover", "hopper", "hugle", "hypatia", "ishizaka", "jackson", "jang", "jemison", "jepsen", "johnson", "joliot", "jones", "kalam", "kamminga", "kare", "keldysh", "keller", "khorana", "kilby", "kirch", "knuth", "kowalevski", "lalande", "lamarr", "lamport", "leakey", "leavitt", "lederberg", "lehmann", "lewin", "liskov", "lovelace", "lumiere", "mahavira", "margulis", "matsumoto", "maxwell", "mayer", "mccarthy", "mcclintock", "mclaren", "mclean", "mcnulty", "mendel", "mendeleev", "meitner", "meninsky", "merkle", "mestorf", "mirzakhani", "morse", "murdock", "moser", "napier", "nash", "neumann", "newton", "nightingale", "nobel", "noether", "northcutt", "noyce", "panini", "pare", "pascal", "pasteur", "payne", "pearce", "perez", "perlis", "pickering", "poincare", "poitras", "popov", "presper", "qian", "raman", "ramanujan", "ride", "montalcini", "ritchie", "rhodes", "roentgen", "rosalind", "roshan", "saha", "sakharov", "shamir", "shannon", "shaw", "shirley", "shockley", "shtern", "sinoussi", "snyder", "solomon", "spence", "standish", "stallman", "stonebraker", "sutherland", "swanson", "swartz", "swirles", "taussig", "tereshkova", "tesla", "thompson", "torvalds", "tu", "turing", "varahamihira", "vaughan", "visvesvaraya", "volhard", "villani", "vinogradov", "virchow", "volterra", "wescoff", "wiles", "williams", "williamson", "wilson", "wing", "wozniak", "wright", "wu", "yalow", "yonath", "zhukovsky"}
)

// GetRandomName generates a random container name.
func GetRandomName() string {
	return getRandom(left[:], right[:])
}

func getRandom(left []string, right []string) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return strings.ToLower(left[r.Intn(len(left))]) + "_" + strings.ToLower(right[r.Intn(len(right))])
}
