package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
	"gitlab.com/jimboylabs/deployer/namesgenerator"
	"golang.org/x/sync/errgroup"
)

func main() {
	app := pocketbase.New()

	// serves static files from the provided public dir (if exists)
	app.OnBeforeServe().Add(func(e *core.ServeEvent) error {
		e.Router.GET("/*", apis.StaticDirectoryHandler(os.DirFS("./pb_public"), true))
		return nil
	})

	app.OnRecordBeforeCreateRequest("deployments").Add(func(e *core.RecordCreateEvent) error {
		var candidateName string

		for {
			candidateName = namesgenerator.GetRandomName()

			result := struct {
				Count int
			}{}

			err := app.Dao().DB().
				NewQuery("SELECT COUNT(*) AS count FROM deployments WHERE name = {:name}").
				Bind(dbx.Params{
					"name": candidateName,
				}).
				One(&result)
			if err != nil {
				return err
			}

			if result.Count == 0 {
				break
			}
		}

		if candidateName == "" {
			return apis.NewApiError(http.StatusInternalServerError, "namesgenerator error", candidateName)
		}

		e.Record.Set("name", candidateName)

		return nil
	})

	g, _ := errgroup.WithContext(context.Background())

	g.Go(func() error {
		return app.Start()
	})

	err := g.Wait()
	if err != nil {
		log.Fatal(err)
	}
}
