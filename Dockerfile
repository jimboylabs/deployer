FROM node:20 AS uibuilder

WORKDIR /app

COPY ./webui/package.json ./webui/package-lock.json ./

RUN npm install

COPY ./webui .

RUN npm run build

# Stage 1: Build the Go application
FROM golang:1.21.1 AS builder

WORKDIR /app

# Copy Go project source code into the container
COPY . .

# Build the Go application
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main .

# Stage 2: Create a minimal image using Distroless Alpine
FROM gcr.io/distroless/base-debian12:latest

WORKDIR /app

# Copy the binary built in the previous stage into the final image
COPY --from=builder /app/main .
COPY --from=uibuilder /app/dist ./pb_public

EXPOSE 8080

# Define the command to run your application
CMD ["./main", "serve", "--http=0.0.0.0:8080"]
